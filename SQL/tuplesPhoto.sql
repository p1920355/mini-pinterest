INSERT INTO Categorie VALUES(0, "animaux");
INSERT INTO Categorie VALUES(1, "paysage");
INSERT INTO Categorie VALUES(2, "art");
INSERT INTO Categorie VALUES(3, "portrait");

INSERT INTO Photo VALUES(0, "éléphant", "Un éléphant d'Asie sous la lumière des sous-bois", 0);
INSERT INTO Photo Values(1, "gratte ciel", "Un quartier fluvial rempli de gratte-ciel", 1);
INSERT INTO Photo Values(2, "renard", "Un renard roux sur de la neige regardant l'objectif", 0);
INSERT INTO Photo VALUES(3, "renard neige", "Un jeune renard des neiges regardant l'objectif", 0);
